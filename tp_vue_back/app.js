const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Product = require('./models/product');
const cors = require('cors');

// Connect to MongoDB
mongoose.connect('mongodb://localhost:27017/webshop', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Initialize app
const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

// Routes
app.get('/products', async (req, res) => {
    const products = await Product.find();
    res.send(products);
});

app.post('/products', async (req, res) => {
    const product = new Product(req.body);
    await product.save();
    res.send(product);
});

// Export app
module.exports = app;
